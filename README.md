# active_mq_java_sample

download and install [ActiveMQ](http://activemq.apache.org/)

## Start ActiveMQ

   ```cd apache-activemq-[version] ```

   ```bin\activemq start```

Place the activemq-all-[version].jar file in the directory with this code.

## Compile

```javac -cp ".;activemq-all-5.13.4.jar;" Producer.java```

```javac -cp ".;activemq-all-5.13.4.jar;" ServerManager.java```

```javac -cp ".;activemq-all-5.13.4.jar;" Consumer.java```

```javac -cp ".;activemq-all-5.13.4.jar;" TestServerManager.java```



Run TCP and UDP Server:

```java -cp ".;activemq-all-5.13.4.jar;" TestServerManager  ```




## Testing

Test adding data to queues:

TCP: telnet to port 4321 (or other ports listed in TestServerManager). Lines entered into the telnet session are forwarded as messages to ActiveMQ

UDP: use the sample program UdpTest.java

Verify by logging into the admin console http://127.0.0.1:8161/admin/queues.jsp   (admin/admin)

Test getting data from queues:

```java -cp ".;activemq-all-5.13.4.jar;" Consumer [queue_name] ```


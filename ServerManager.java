import java.util.Map;
import java.util.*;

public class ServerManager {
    private Map<String, Producer> servers = new HashMap<String, Producer>();

    public void stop(String serverName) {
        Producer server = servers.get(serverName);
        server.kill();
    }

    public void buildServer(String serverName, String type, int portNumber) {

        System.out.println("creating " + serverName + " port: " + portNumber );
        Producer new_server = new Producer(serverName, type, portNumber);
        System.out.println("starting" );
        new_server.myStart();
        servers.put(serverName, new_server);
    }

    public void killAll(){
        for (String key : servers.keySet()) {
            Producer server = servers.get(key);
            server.kill();
        }
    }
}

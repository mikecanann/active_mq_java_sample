import java.io.*;
import java.net.*;


class TestServerManager
{


    public static void main(String args[]) throws Exception
    {
//        if (args.length != 1) {
//            System.err.println("Usage: java this_file <param>");
//            System.exit(1);
//        }


        ServerManager sm = new ServerManager();


        sm.buildServer("tcp1", "tcp", 4321);
        sm.buildServer("tcp2", "tcp", 4322);
        sm.buildServer("tcp3", "tcp", 4323);

        System.out.println("created TCP servers");

        Thread.sleep(10000); // sleep 10 seconds

        sm.buildServer("udp1", "udp", 4324);
        sm.buildServer("udp2", "udp", 4325);

        System.out.println("created UDP servers");
        Thread.sleep(10000); // sleep 10 seconds

        sm.stop("tcp1");
        sm.stop("udp1");

        System.out.println("stopping some servers");

        Thread.sleep(500000); // sleep 500 seconds

        System.out.println("stopping all servers");

        sm.killAll();

    }
}


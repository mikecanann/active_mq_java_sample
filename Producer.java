

// ActiveMQ
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;

// socket listener
import java.net.*;
import java.io.*;

public class Producer implements Runnable {


    private int portNumber = 4321;
    private String serverName = "";
    private String type = "";


    private volatile boolean keepRunning = true ;


    // URL of the JMS server. DEFAULT_BROKER_URL is localhost
    private static String url = ActiveMQConnection.DEFAULT_BROKER_URL;
    // default broker URL is : tcp://localhost:61616"


    public Producer(String svName, String tp, int portNum){
        serverName = svName;
        portNumber = portNum;

        type = tp;

    }

    public void run()  {

        System.out.println("starting run for server " + serverName + " port: " + portNumber );
        if(type.equals("tcp")) {
            try{
                run_tcp_server();
            }
            catch (IOException|JMSException e) {
            }
        }
        else {
            try{
                run_udp_server();
            }
            catch (IOException|JMSException e) {
            }
        }
    }

    public void kill() {
        keepRunning = false ;
    }

    public void myStart() {
        // check if already running
        Thread serverThread = new Thread(this);

        serverThread.start();

    }

    /*
    public ServerSocket getTcpService() {
        return tcpService;
    }

    public void setTcpService(ServerSocket tcpService) {
        this.tcpService = tcpService;
    }

    public DataInputStream getInputStream() {
        return inputStream;
    }

    public void setInputStream(DataInputStream inputStream) {
        this.inputStream = inputStream;
    }

    public PrintStream getOutput() {
        return output;
    }

    public void setOutput(PrintStream output) {
        this.output = output;
    }
    */

    public static void send_active_mq_message(String subject, String msg) throws JMSException {
        // Getting JMS connection from the server and starting it
        ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(url);
        Connection connection = connectionFactory.createConnection();
        connection.start();

        // JMS messages are sent and received using a Session. We will
        // create here a non-transactional session object. If you want
        // to use transactions you should set the first parameter to 'true'
        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

        // the queue will be created if it doesn't exist
        Destination destination = session.createQueue(subject);

        // MessageProducer for sending messages
        // MessageConsumer for receiving messages
        MessageProducer producer = session.createProducer(destination);

        // Create the message
        TextMessage message = session.createTextMessage(msg);

        // post the message to the queue
        producer.send(message);
        System.out.println("sent '" + message.getText() + "'");

        connection.close();
    }


    public  void run_udp_server() throws JMSException, IOException  {



        DatagramSocket serverSocket = new DatagramSocket(portNumber);
        byte[] receiveData = new byte[1024];
        byte[] sendData = new byte[1024];

        while(keepRunning) {
                System.out.println("keeprunning ");

            try {
                DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
                serverSocket.receive(receivePacket);

                byte[] data = receivePacket.getData();
                String sentence = new String(data, 0, receivePacket.getLength());


                System.out.println("RECEIVED: " + sentence);

                // add to the queue - will create the queue 'serverName' if it doesn't exist
                send_active_mq_message(serverName, sentence);

                InetAddress IPAddress = receivePacket.getAddress();
                int port = receivePacket.getPort();

                sendData = sentence.getBytes();
                DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, port);
                serverSocket.send(sendPacket);
            } catch (IOException e) {
                System.out.println("Exception caught when trying to listen on port "
                    + portNumber + " or listening for a connection");
                System.out.println(e.getMessage());

            }

        }
    }

    //public  void run_tcp_server() throws JMSException, IOException  {
    public  void run_tcp_server() throws JMSException, IOException  {




        ServerSocket serverSocket = new ServerSocket(portNumber);


        while(keepRunning) {

            try (
                Socket clientSocket = serverSocket.accept();
                PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
                BufferedReader in = new BufferedReader( new InputStreamReader(clientSocket.getInputStream()));
            ) {
                String inputLine;
                while ((inputLine = in.readLine()) != null) {

                    // add to the queue - will create the queue 'serverName' if it doesn't exist
                    send_active_mq_message(serverName, inputLine);

                    out.println(inputLine);
                }
            } catch (IOException e) {
                System.out.println("Exception caught when trying to listen on port "
                    + portNumber + " or listening for a connection");
                System.out.println(e.getMessage());
            }
        }
    }

}
